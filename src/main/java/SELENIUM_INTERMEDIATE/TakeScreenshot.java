package SELENIUM_INTERMEDIATE;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TakeScreenshot {

	public static void main(String[] args) throws InterruptedException, AWTException, IOException {

		
		

	  	  System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));

			WebDriver driver = new ChromeDriver();
			
			driver.get("https://www.google.com");
			Thread.sleep(1000);
			driver.findElement(By.id("lst-ib")).sendKeys("amazon");
			Thread.sleep(1000);

			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ESCAPE);
			robot.keyRelease(KeyEvent.VK_ESCAPE);
			WebElement searchbuton= driver.findElement(By.name("btnK"));
			searchbuton.click();
			Thread.sleep(1000);
			
			File file= new File(System.getProperty("user.dir").concat("\\SCREENSHOT\\Screen1.png"));
		    File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		    FileUtils.copyFile(scrFile, file);
			driver.quit();
	}

}

