package SELENIUM_INTERMEDIATE;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class DragAndDrop {

	public static void main(String[] args) throws InterruptedException {

	System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));
	WebDriver  driver=new ChromeDriver();
		driver.get("http://demoqa.com/draggable/");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"ui-id-5\"]")).click();
		
		WebElement item1=driver.findElement(By.xpath("//*[@id=\"sortablebox\"]/li[1]"));
	
	//	WebElement item2=driver.findElement(By.xpath("//*[@id=\"sortablebox\"]/li[1]"));

		
	Actions a = new Actions(driver);
	
	a.dragAndDrop(item1,driver.findElement(By.xpath("//*[@id=\"sortablebox\"]/li[5]"))).release().build().perform();
	
	
	}

}
