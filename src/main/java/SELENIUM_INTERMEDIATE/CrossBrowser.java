package SELENIUM_INTERMEDIATE;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class CrossBrowser {
	static WebDriver driver;
	public static void main(String[] args) throws IOException {
		
		 FileInputStream fi=new FileInputStream(System.getProperty("user.dir").concat("\\PROPERTIES\\BrowserType.properties"));
		  Properties property=new Properties();
		   property.load(fi);
		String browser=property.getProperty("browserName");
		switch(browser)
		{
			case "chrome": 
				  System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));
				  driver=new ChromeDriver();
				  break;  
			case "edge": 
				  System.setProperty("webdriver.edge.driver",System.getProperty("user.dir").concat("\\Drivers\\MicrosoftWebDriver.exe"));
				  driver=new EdgeDriver();
				  break;
			case "headless":
				  driver = new HtmlUnitDriver(); 
				  break;
		}	    
				    
		
		
	String URL=property.getProperty("ApplicationUrl");
	driver.get(URL);
	System.out.println("Title of the page is -> " + driver.getTitle());

	}

}
