package SELENIUM_INTERMEDIATE;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class HeadlessBrowser {

	public static void main(String[] args) {

		

		WebDriver driver = new HtmlUnitDriver(); 
		// open google.com webpage
		driver.get("http://google.com");
 
		System.out.println("Title of the page is -> " + driver.getTitle());
 
		
		
	}

}
