package SELENIUM_INTERMEDIATE;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowsHandle {

	public static void main(String[] args) throws InterruptedException {
		
		  System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));

		WebDriver driver = new ChromeDriver();

	        // Put an Implicit wait, this means that any search for elements on the page could take the time the implicit wait is set for before throwing exception

	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	        // Launch the URL

	        driver.get("http://toolsqa.wpengine.com/automation-practice-switch-windows/");

	        // Store and Print the name of the First window on the console

	        String handle= driver.getWindowHandle();

	        System.out.println("First screen handle value is "+handle);

	        // Click on the Button "New Message Window"
	        Thread.sleep(1000);
	      
	        driver.findElement(By.xpath("//*[@id=\"content\"]/p[3]/button")).click();

	        // Store and Print the name of all the windows open	              
	        Thread.sleep(1000);

	        Set handles = driver.getWindowHandles();

	        System.out.println(handles);

	        // Pass a window handle to the other window
	        Thread.sleep(1000);

	        for (String handle1 : driver.getWindowHandles()) {

	        	System.out.println(handle1);

	        	driver.switchTo().window(handle1);

	        	}
	        
	        driver.manage().window().maximize();

	        // Closing Pop Up window
	        Thread.sleep(1000);

	        driver.close();

	        // Close Original window

	       // driver.quit();
		
	}

}
