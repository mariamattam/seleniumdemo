package SELENIUM_INTERMEDIATE;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Wait {

	public static void main(String[] args) throws InterruptedException, AWTException {


	  	   System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));
	  	   Thread.sleep(500);
			WebDriver driver = new ChromeDriver();
			//Implicit wait
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.get("https://www.google.com");
			
			//Explicit wait
			
 WebDriverWait wait=new WebDriverWait(driver,10);
			
	wait.until(ExpectedConditions.presenceOfElementLocated(By.id("lst-ib"))).sendKeys("amazon");
	
			//driver.findElement(By.id("lst-ib")).sendKeys("amazon");
		Thread.sleep(1000);

			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ESCAPE);
			robot.keyRelease(KeyEvent.VK_ESCAPE);
			WebElement searchbuton= driver.findElement(By.name("btnK"));
			wait.until(ExpectedConditions.elementToBeClickable(searchbuton)).click();
		
			//searchbuton.click();
			Thread.sleep(1000);

			driver.quit();
		
		
		
	}

}
