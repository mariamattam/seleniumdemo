package SELENIUM_INTERMEDIATE;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Alerts {
	
	public static void main(String args[]) throws InterruptedException, AWTException
	{
		
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));
		 WebDriver driver=new ChromeDriver();
		 
		 driver.get("http://toolsqa.com/handling-alerts-using-selenium-webdriver/");
		 driver.manage().window().maximize();
	
		 driver.findElement(By.xpath("//*[@id=\"content\"]/p[4]/button")).click();
		 Thread.sleep(1000);
		 Alert alert=driver.switchTo().alert();
		 alert.accept();
		 Thread.sleep(1000); 
		 Robot r = new Robot();
		 r.keyPress(KeyEvent.VK_DOWN);
	     r.keyPress(KeyEvent.VK_DOWN);
	     
	     r.keyPress(KeyEvent.VK_TAB);
	     r.keyPress(KeyEvent.VK_TAB);
		Actions a = new Actions(driver);
		a.moveToElement(driver.findElement(By.xpath("//*[@id=\"content\"]/p[8]/button")));
		WebDriverWait w =new WebDriverWait(driver,10);
		w.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id=\"content\"]/p[8]/button")))).click();
		driver.findElement(By.xpath("//*[@id=\"content\"]/p[8]/button")).click();
		Alert alert1=driver.switchTo().alert();
		 alert1.dismiss();
			 
		 
	}

}
