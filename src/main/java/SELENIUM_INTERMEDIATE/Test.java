package SELENIUM_INTERMEDIATE;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Test {

	// In this program element not found exception is thrown 
	//but still program executes because exception is handled
	
	
	public static void main(String[] args) throws InterruptedException, AWTException {
  
		for(int i=0;i<3;i++)
		{
		
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));

		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.google.com");
		Thread.sleep(1000);
		try {
			//invalid element id----so search field not work and throws element not found exception
			driver.findElement(By.id("lst")).sendKeys("amazon");

		}
		catch(Exception e)
		{
			System.err.println("Exception handled");
		}
		Thread.sleep(1000);
		System.err.println("new linr");
        Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ESCAPE);
		robot.keyRelease(KeyEvent.VK_ESCAPE);
	    driver.findElement(By.name("btnK")).click();
		Thread.sleep(1000);
        System.err.println("error linr");
		driver.quit();
		}
		
	}

}
