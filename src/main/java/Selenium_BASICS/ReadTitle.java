package Selenium_BASICS;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ReadTitle {

	public static void main(String[] args) throws InterruptedException {

		

	  	    System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));

			WebDriver driver = new ChromeDriver();
			
			driver.get("https://www.google.com");
			System.out.println("title of the page is "+driver.getTitle());
			System.out.println("current url of the page is "+driver.getCurrentUrl());
            Thread.sleep(1000);
            if(driver.getTitle().equals("Google"))
            {
            	
            	System.err.println("Test case is passed");
            }
            
			driver.quit();
		
		
		
	}

}
