package Selenium_BASICS;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Search_google {

	public static void main(String[] args) throws  AWTException, InterruptedException {


  	  System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));

		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.google.com");
		Thread.sleep(1000);
		driver.findElement(By.id("lst-ib")).sendKeys("amazon");
		Thread.sleep(1000);

		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ESCAPE);
		robot.keyRelease(KeyEvent.VK_ESCAPE);
		WebElement searchbuton= driver.findElement(By.name("btnK"));
		
		searchbuton.click();
		Thread.sleep(1000);

		driver.quit();
	
	}

}





// get ()--- used to open the browser
//quit ()-- used to close the browser
// Sleep()-- used to make the computer idle
// We can store the element in WebElement object and then perform using the object as mentioned in line 29
// Robot class is used for Keyboard action and mouse action it is inbuild Java class
