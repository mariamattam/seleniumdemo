package Selenium_BASICS;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class ISDISPLAYED {

	public static void main(String[] args) throws InterruptedException {


		  System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));

			WebDriver driver = new ChromeDriver();
			
			driver.get("https://www.google.com");
			
			//driver.get("https://www.google.com");
			Thread.sleep(1000);
			WebElement Searchbox=driver.findElement(By.id("lst-ib"));
			//boolean condition=Searchbox.isDisplayed();

		if(Searchbox.isDisplayed())
		{
			System.err.println("search box is displayed in the screen");
			System.err.println("Test case is passed");

		}
		else {
			System.err.println("google screen search box is displayed in the screen");
			System.err.println("Test case is failed");

		}
			
		
		Searchbox.sendKeys("sachin");
		String ActualValue=Searchbox.getAttribute("value");
		
	if(ActualValue.equals("sachin"))
	{
		System.err.println("Test case is passed");

		System.err.println("search field is typed with word sachin");
	
	}
		
	}
	
	
	//similary isenabled() method is used to check whether element is enabled or not
	//similary isselected() method is used to check whether element is selected or not

}
