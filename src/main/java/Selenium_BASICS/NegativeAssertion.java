package Selenium_BASICS;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class NegativeAssertion {

	public static void main(String[] args) throws InterruptedException {

		

		  System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));

			WebDriver driver = new ChromeDriver();
			
			driver.get("https://www.google.com");
			
			String s1="Google123";
			
			String s2=driver.getTitle();
			Assert.assertEquals(s1, s2);			
            Thread.sleep(1000);
			driver.quit();
		
		
		//Assert()-- used for verification
		
		// Assert equals -- returns error, if actual and expected value is different
		
	}

}
