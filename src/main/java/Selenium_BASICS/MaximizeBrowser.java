package Selenium_BASICS;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class MaximizeBrowser {

	public static void main(String[] args) throws AWTException, InterruptedException {

		
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));
   System.err.println(System.getProperty("user.dir"));
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.google.com");
		
		driver.manage().window().fullscreen();
		Thread.sleep(1000);

		
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_F11);
		robot.keyRelease(KeyEvent.VK_F11);
		
		Thread.sleep(1000);

		driver.manage().window().maximize();
		
		Thread.sleep(1000);

		driver.quit();
		
	}

}
