package Selenium_BASICS;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;


public class Assertion {

	public static void main(String[] args) throws InterruptedException {
		
		
		System.err.println(System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));
		System.err.println(System.getProperty("user.dir"));

		  System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));

			WebDriver driver = new ChromeDriver();
			
			driver.get("https://www.google.com");
			
			String s1="Google";
			
			String s2=driver.getTitle();
			Assert.assertEquals(s1, s2);	
          Thread.sleep(1000);
			driver.quit();
		
		
		//Assert()-- used for verification
		
		// Assert equals -- returns no value, if both values are equal
	}

}
