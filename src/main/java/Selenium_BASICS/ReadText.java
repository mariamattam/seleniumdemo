package Selenium_BASICS;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ReadText {

  public static void main(String[] args) throws InterruptedException {


	    System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));

		WebDriver driver = new ChromeDriver();
		driver.get("https://demoqa.com");
		
	/*Absolute xpath*/
		
	String Text=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/main/article/header/h1")).getText();
	
	/*Relative xpath*/	
		
	//String Text=driver.findElement(By.xpath("//main/article/header/h1")).getText();
 
	/* Single Attribute */
 //String Text=driver.findElement(By.xpath("//h1[@class='entry-title']")).getText();
// another option    *[@attribute_name='attribute_value']
 
System.out.println("My text is "+Text);
 Thread.sleep(1000);
/*String s2=driver.findElement(By.xpath("//a[@title='Services'][@href='http://demoqa.com/services/']")).getText();

System.out.println("Link text is "+s2);
*/
 
	}

}


// Here I Have used absolue xpath