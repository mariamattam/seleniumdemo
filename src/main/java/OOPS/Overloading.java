package OOPS;

public class Overloading {

	public static void main(String[] args) {

	draw(5);
	draw(5,4);

		
	}
	
	public static void draw(int radius)
	{
		int area=(int) (2.17*radius*radius);
		
		System.out.println("Area of circle is" +area);
	}
	
	public static void draw(int width,int height)
	{
		int area=width*height;
		
		System.out.println("Area of Rectangle is" +area);
	}


}
// This method name is called overloading 
// same method is defined more than one time but different parament