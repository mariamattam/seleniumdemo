package Basics;

public class Reinitializing {

	static int a;
	public static void main(String[] args) {

		a=10;
		System.err.println("value of a is "+a);
		
		second();
		third();
		
	}

	public static void second()
	{
		a=15;
		System.err.println("value of a is "+a);
		
	}
	
	public static void third()
	{
		a=20;
		System.err.println("value of a is "+a);

	}

	// Here we are reinitializing the value of static variable
	//last assigned value will be the current value of static variable a
	
}
