package Basics;

public class LocalVariable {
	
	public static void main(String args[])
	
	{
		local();
		
	}

	public static void local()
	
	{
		int a=5;
		System.out.println("local variable a is"+a);
		
	}
}

// a is local variable and it cannot be used outside the method local
// a is local variable because a is initialized and declared within the method local