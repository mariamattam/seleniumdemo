package Basics;

public class Types {

	static int a=10;
	static boolean value=true;
	
	char letter='a';
	
	public static void main(String[] args) {
		System.out.println("static value of a is "+a);
		System.out.println("added with a is "+5+a);
		
    call();
    type2.mystatic();

	}
	
	public static void call()
	{
		Types t=new Types();
		System.out.println("my letter is"+t.letter);
		t.call2();
		
	}
	public void call2()
	{
		
		System.out.println("Instance method is called");
	}
	

}	
	
class type2
{
	
	public static void mystatic()
	{
		
		System.out.println("static method will be  called by outside the class");
        System.out.println("calling static variable from outside class "+Types.a);
		Types t=new Types();
        System.out.println("calling instance variable from outside class "+t.letter);

	}
	
}

// Here calling instance variable, static variable inside and outside the class
//Calling instance method and static method inside and outside the class



