package Basics;

public class MultipleInstance {

	public static void main(String[] args) {

		MultipleInstance m1=new MultipleInstance();
		MultipleInstance m2=new MultipleInstance();
		MultipleInstance m3=new MultipleInstance();

		m1.display();
		m2.display();
		m3.display();
		
	}
	
	public void display()
	{
		System.out.println("object called me");
		
	}

}
// each class can have multiple object creation