package Basics;

public class Methodcalling {

	public static void main(String[] args) {
      System.err.println("Main method trying to execute method call");
		call();

	}

	
	public static void call()
	{
		
	      System.err.println("Executing call method");

		call1();
		

	}
	
	public static void call1()
	{
	      System.err.println("Executing call 1 method");
		Methodcalling c=new Methodcalling();
		c.call2();

	}
	
	public void call2()
	{
	      System.err.println("Executing call 2 method");
		
	}
}
