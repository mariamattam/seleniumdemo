package Basics;

public class Final {

	final static int a = 10;
	
	public static void main(String[] args) {

		System.err.println("value of a cannot be changed  " +a);
		
	}

}
// if final keyword is used before the variable, the value of the variable cannot be changed