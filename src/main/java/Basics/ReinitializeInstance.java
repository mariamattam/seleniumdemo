package Basics;

public class ReinitializeInstance {
  
	 int a=10;
	 int b;
	public static void main(String[] args) {

   ReinitializeInstance r1 =new ReinitializeInstance();
   r1.a=20;
	System.out.println( "value of a is   "+r1.a);
	r1.call();
	r1.call2();
	}

	
	
	
	public void call()
	
	{   ReinitializeInstance r1 =new ReinitializeInstance();

	System.out.println( "value of  a is   "+r1.a);
	   r1.a=50;

		r1.b=20;
	}
	public void call2()
	
	{   ReinitializeInstance r1 =new ReinitializeInstance();
	    ReinitializeInstance r2 =new ReinitializeInstance();

	System.out.println( "value of a  is   "+r1.a);

	System.out.println( "value of  a with another object is   "+r2.a);

	}
	
}

// Each object created inside the method holds value for the instance variable separately
//So reinitializing is not generic, ie( a instance variable can have different value, based on the object created)
//
