package listen;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class Results implements ITestListener {

	public void onTestStart(ITestResult result) {

		System.err.println("System is going to execute Test case"+result.getName());
		
	}

	public void onTestSuccess(ITestResult result) {

		
		System.err.println("-------Test case  is passed------"+result.getName());

		
	}

	public void onTestFailure(ITestResult result) {

		
		System.err.println("-------Test case  is failed-------"+result.getName());

		
	}

	public void onTestSkipped(ITestResult result) {

		System.err.println("---------Test case  is skipped---------"+result.getName());

		
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
		
	}

	public void onStart(ITestContext context) {
		
		
		
		
		// TODO Auto-generated method stub
		
	}

	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

}
