package Error;

public class Outsideclass {

	public static void main(String[] args) {

		
		check();
	}

}

class secondary
{
	
	public static void check()
	
	{
		
		System.out.println("this program wont run");
	}
}

// this program wont run because static method is called directly outside the class

// you should call the static method using class name 

//eg: secondary.check();