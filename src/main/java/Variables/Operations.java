package Variables;

public class Operations {

	public static void main(String[] args) {

		new Operations().calculations();
		
		//new Operation()   ----- is another way of object creation and calling the method directly
        // here there is no need to store reference variable
		
		
		Operations a = new Operations();
		a.equal();
	
	}
	
	public void calculations()
	{
		
		int c= 10+20;
		System.out.println("value of c is "+c);
		int d=20-10;
		int f=20*10;
		int k=20/2;
		System.out.println("value of d is "+d);
		System.out.println("value of f is "+f);
		System.out.println("value of k is "+k);

		/*       =   is asignment operator , used to store right hand side value to left hand side variable
        +   is an addition operator
        -   is subtraction operator
        /   is division operator
*/
		
		
	}
	
	
	void equal()
	{  
		int a=10;
		if(a==10)
		{
			System.out.println("value is equal");
			
			// == operator is used to check whether left side  value and right side value is equal
			
		}
		
		
	}

}
