package Variables;

public class StringMethods {

	public static void main(String[] args) {

		String s1=new String("java");
	
		int stringLength=s1.length();
		System.out.println("length of the string is "+stringLength);
		
		
		String words="THIS IS GREAT";
		System.out.println( "Actual String is "+words);
		
		System.out.println( words.toLowerCase() );
	}

}

/* in this program we can learn two type of String creation
 *  Type 1:    String s1=new String(" " );
 *  Type 2:    String s1 ="ganesh";
 
 String consist of more inbuild functions
 
 example   :    length()  --- returns length of the string
                 toLowercase--- converts the string to lowercase and return that String"
                 
                 */
