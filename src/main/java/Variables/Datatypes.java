package Variables;

public class Datatypes {

	public static void main(String[] args) {

		int a=10;
		int myvariable=50;
		float data=0.2f;
		char letter='c';
		boolean check=true;
		long bignum=65236523;
		//int a1=2738927389723;
		short smallnum=10121;
		double a2=0.2f;
		
		System.out.println(a);

		System.out.println("Value of a is"+a);
		System.out.println("Value of My variable is "+myvariable);
		System.out.println("Value of data is "+data);
		System.out.println("value of letter is "+letter);
		System.out.println(check);
		System.out.println(bignum);
		System.out.println(smallnum);
		System.out.println(a2);

/*		From this program , we can understand types of Variables and data types we can Use

*      variable is a name used to store some value
*      
*      DataType is used before the variable name, so that similar type of value is stored
*/		
		

		
	}

}
