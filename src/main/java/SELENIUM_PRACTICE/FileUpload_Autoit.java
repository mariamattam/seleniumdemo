package SELENIUM_PRACTICE;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FileUpload_Autoit {

	public static void main(String[] args) throws IOException {

		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));

		WebDriver driver = new ChromeDriver();
		driver.get("http://demoqa.com/");
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//*[@id=\"menu-item-374\"]/a")).click();
		
		driver.findElement(By.xpath("//*[@id=\"pie_register\"]/li[14]/div/input")).click();
		driver.findElement(By.xpath("//*[@id=\"profile_pic_10\"]")).click();
        Runtime.getRuntime().exec(System.getProperty("user.dir").concat("\\AutoITSCRIPT\\upload.exe"));

		
	}

}
