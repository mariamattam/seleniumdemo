package SELENIUM_PRACTICE;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Move {

	public static void main(String[] args) throws InterruptedException {

	  	  System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));

			WebDriver driver = new ChromeDriver();

			driver.get("http://demoqa.com/");
			
WebElement box1=driver.findElement(By.xpath("//*[@id=\"post-9\"]/div/div[1]/div/p[1]/a/img\r\n" ));
Thread.sleep(1000);

Actions a=new Actions(driver);
a.moveToElement(box1).build().perform();



WebElement box2=driver.findElement(By.xpath("//*[@id=\"post-9\"]/div/div[2]/div/p[1]/i/a/img" ));

Thread.sleep(1000);
Actions a1=new Actions(driver);
a1.moveToElement(box2).build().perform();


WebElement box3=driver.findElement(By.xpath("//*[@id=\"post-9\"]/div/div[3]/div/i/a/img"));

Thread.sleep(1000);
Actions a2=new Actions(driver);
a2.moveToElement(box3).click().build().perform();



	}

}
