
package SELENIUM_PRACTICE;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Form {

	public static void main(String[] args) throws IOException, AWTException, InterruptedException {
		// TODO Auto-generated method stub
		
		
	  	   System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));

			WebDriver driver = new ChromeDriver();


		driver.get("http://demoqa.com/");
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//*[@id=\"menu-item-374\"]/a")).click();
		driver.findElement(By.xpath("//*[@id=\"name_3_firstname\"]")).sendKeys("VIVEK");
		driver.findElement(By.xpath("//*[@id=\"name_3_lastname\"]")).sendKeys("RAJAN");
		driver.findElement(By.xpath("//*[@id=\"pie_register\"]/li[2]/div/div/input[1]")).click();
	 List<WebElement> radiobuttons =driver.findElements(By.xpath("//*[@id=\"pie_register\"]/li[2]/div/div/input"));
		driver.findElement(By.xpath("//*[@id=\"pie_register\"]/li[3]/div/div/input[3]")).click();
	 
	System.err.println(radiobuttons.size()); 
	radiobuttons.get(1).click();
	
	
	
	boolean radiocheck=radiobuttons.get(1).isSelected();
	System.err.println(radiocheck);
	
	Select country = new Select(driver.findElement(By.xpath("//*[@id=\"dropdown_7\"]")));
	country.selectByVisibleText("India");
	
	Select month = new Select(driver.findElement(By.xpath("//*[@id=\"mm_date_8\"]")));
	month.selectByValue("8");
	
	Select year = new Select(driver.findElement(By.xpath("//*[@id=\"yy_date_8\"]")));
	year.selectByValue("1987");
	
	Select day = new Select(driver.findElement(By.xpath("//*[@id=\"dd_date_8\"]")));
	day.selectByValue("19");
	
	driver.findElement(By.xpath("//*[@id=\"phone_9\"]")).sendKeys("9789013425");
	driver.findElement(By.xpath("//*[@id=\"username\"]")).sendKeys("VivekShettyRajan");
	driver.findElement(By.xpath("//*[@id=\"email_1\"]")).sendKeys("vivekshettyrajan@gmail.com");
	
	
	
	


	driver.findElement(By.xpath("//*[@id=\"profile_pic_10\"]")).click();

	setClipboardData("C:\\Users\\Ganesh-Adya Sports\\Pictures\\jenkins_image.png");

	
	ctrl_v();
	
	driver.findElement(By.xpath("//*[@id=\"description\"]")).sendKeys("Selenium Training");
	driver.findElement(By.xpath("//*[@id=\"password_2\"]")).sendKeys("Password@123$#");
	driver.findElement(By.xpath("//*[@id=\"confirm_password_password_2\"]")).sendKeys("Password@123$#");
	
	driver.findElement(By.xpath("//*[@id=\"pie_register\"]/li[14]/div/input")).click();
	
	}
	
	
	
	public static void setClipboardData(String string) {
		//StringSelection is a class that can be used for copy and paste operations.
		   StringSelection stringSelection = new StringSelection(string);
		   Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		}

	public static void ctrl_v() throws AWTException, InterruptedException
	{
		Thread.sleep(10000);
		 Robot robot=new Robot();
		  robot.keyPress(KeyEvent.VK_CONTROL);
	      robot.keyPress(KeyEvent.VK_V);
	      robot.keyRelease(KeyEvent.VK_CONTROL);
	      robot.keyRelease(KeyEvent.VK_V);
	       robot.keyPress(KeyEvent.VK_ENTER);
	       robot.keyRelease(KeyEvent.VK_ENTER);
		
	}

	public static void KeyboardEnter() throws AWTException, InterruptedException
	{
		
		 Robot robot=new Robot();
	       robot.keyPress(KeyEvent.VK_ENTER);
	       robot.keyRelease(KeyEvent.VK_ENTER);
		
	}
	}