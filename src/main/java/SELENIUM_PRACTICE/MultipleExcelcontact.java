package SELENIUM_PRACTICE;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class MultipleExcelcontact {
	static FileInputStream excelfile;
	static XSSFWorkbook wbook;
	static XSSFSheet wsheet;
	public static void main(String[] args) throws IOException, InterruptedException {

		excelfile = new FileInputStream(System.getProperty("user.dir").concat("\\EXCEL\\input.xlsx"));
		 wbook = new XSSFWorkbook(excelfile);
	     wsheet= wbook.getSheet("contactus");
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));
      
        for(int i=1;i<=wsheet.getLastRowNum();i++)
        {
            WebDriver driver = new ChromeDriver();

        driver.get("http://demoqa.com/");
		driver.manage().window().maximize();

		driver.findElement(By.linkText("Contact")).click();
		
		wsheet.getRow(i).getCell(0).setCellType(Cell.CELL_TYPE_STRING);
		String name=wsheet.getRow(i).getCell(0).getStringCellValue();
		
		driver.findElement(By.name("your-name")).sendKeys(name);
		
		wsheet.getRow(i).getCell(1).setCellType(Cell.CELL_TYPE_STRING);
		String email=wsheet.getRow(i).getCell(1).getStringCellValue();
		
		driver.findElement(By.name("your-email")).sendKeys(email);
         
		wsheet.getRow(i).getCell(2).setCellType(Cell.CELL_TYPE_STRING);
		String subject=wsheet.getRow(i).getCell(2).getStringCellValue();

		
		driver.findElement(By.name("your-subject")).sendKeys(subject);
		
		wsheet.getRow(i).getCell(3).setCellType(Cell.CELL_TYPE_STRING);
		String message=wsheet.getRow(i).getCell(3).getStringCellValue();
		
		driver.findElement(By.name("your-message")).sendKeys(message);
		
	
		driver.findElement(By.xpath("//*[@id=\"wpcf7-f375-p28-o1\"]/form/p[5]/input")).click();
Thread.sleep(1000);
		
		driver.quit();
        }
	}

}
