package Conditional;

public class Ifelse {

	public static void main(String[] args) {

		
		float a=10.0f;
		float b=2.f;
		
		if(a==b)
		{
			System.err.println("I wont execute if numbers are not equal");
		}
		
		//end of if
		
		if(a!=b)
		{
			System.err.println("A and B are not equal");

		}
		//end of if

		
		if(a==b)
		{
			System.err.println("A and B are  equal------");

		}
		else
		{
			System.err.println("A and B are not equal-----");

		}
		
	}

}
// In this program == operator is used to compare two side value and then returns true or false