package Conditional;

public class For {

	public static void main(String[] args) {

		for(int i=1;i<=10;i++)
		
		{
			
			System.err.println("Welcome to Java");
			
		}
		
		// below code is called "for each"
		int a[]= {1,2,3};
		for(int b:a )
		{
			
			System.err.println("value of array"+b);
	
		}
		
	}

}
// for is used for executing the loop continusly based on the condition

// second for is called as for each
// to use For each  we should have some list of values in a variable,
//then we can iterate each value in a local variable