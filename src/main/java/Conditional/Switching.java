package Conditional;

public class Switching {

	public static void main(String[] args) {

		char c ='d';
		
		switch(c)
		
		{
		
		case 'c': System.out.println("I am first case "+c);
			     break;
			
		case 'f':System.out.println("I am second case "+c);
	             break;

	
		case 'd':System.out.println("I am third case "+c);
	            break;

				}
		
		
	}

}


//Switch is something you can pass some value
// if the value matches in any one of the case value, the the particular case block will execute