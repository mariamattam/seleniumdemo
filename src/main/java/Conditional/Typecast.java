package Conditional;

public class Typecast {

	public static void main(String[] args) {

		float b=2.3f;
		
		int a =(int) b;
		System.out.println("value of float can be converted as int and then stored in a "+a);
		
		
		String number ="10";
		
		int mynumber=Integer.parseInt(number);
		System.out.println("String is converted in to Integer " +mynumber);
	}

}




// System.out.println("value of float can be converted as int and then stored in a");
// this is called type casting