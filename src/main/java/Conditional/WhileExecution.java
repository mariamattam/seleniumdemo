package Conditional;

public class WhileExecution {

	public static void main(String[] args) {

		int a=1;
		boolean b=true;
		while(b)
		{
			System.err.println("I am executed");
			
			if(a==3)
			{
				b=false;
			}
			a++;
		}
		
	}

}
// while statement will check if the condition passed is true, if it is true, then execute until the condition becomes false
// a++ will increment the value of a

// Similary do while statement is there , please google and explore the concept