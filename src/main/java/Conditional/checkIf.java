package Conditional;

public class checkIf {

	public static void main(String[] args) {

		if(true)
		{
			System.out.println("condition is true");
		}
		
		
		if(false)
		{
			System.out.println("I wont get executed");

		}
		else {
			
			System.out.println("condition is false");

		}
		
	}

}
// so  we can come to know that if only accepts true or false