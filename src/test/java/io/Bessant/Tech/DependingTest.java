package io.Bessant.Tech;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;

public class DependingTest {

 @BeforeMethod
  public void beforeMethod() {
  }
  
  @Test(dependsOnMethods= {"second"})
  public void first() 
  {
	  System.err.println("first");
  
  }
  
  @Test
  public void second() {
	  System.err.println("second");

	 throw new SkipException("skipped");
	  
  }

  @AfterMethod
  public void afterMethod() {
  }

}
