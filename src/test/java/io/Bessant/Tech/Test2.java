package io.Bessant.Tech;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;

public class Test2 {  
  @BeforeMethod
  public void beforeMethod() {
	 System.err.println("system is going to run Second Test case");
}
  
  @Test
  public void Test3() {
	  WebDriver  driver = new ChromeDriver();
		driver.get("https://www.facebook.com");
	  System.err.println("system is running Second Test case");

  }

  @AfterMethod
  public void afterMethod() {
	  
	  System.err.println("system is Executed Second Test case");

  }

}
