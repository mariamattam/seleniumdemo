package io.Bessant.Tech;

import org.testng.annotations.Test;


import listen.SeleniumListeners;

import org.testng.annotations.BeforeMethod;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterMethod;

public class SeleniumEvent {
	
	  WebDriver d;
	  SeleniumListeners d1;
	  EventFiringWebDriver driver;
	  @BeforeMethod
	  public void beforeMethod() throws InterruptedException {
		  
		  System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));

		  d = new ChromeDriver();
		  
		  d1=new SeleniumListeners();
		  driver=new EventFiringWebDriver(d);
		  driver.register(d1);

		  driver.get("https://www.google.com");
		  Thread.sleep(1000);
		  
		  
	  }

  @Test
  public void f() throws InterruptedException, AWTException {
	  
	  driver.findElement(By.id("lst-ib")).sendKeys("amazon");
	  Thread.sleep(1000);

	  Robot robot = new Robot();
	  robot.keyPress(KeyEvent.VK_ESCAPE);
	  robot.keyRelease(KeyEvent.VK_ESCAPE);
	  WebElement searchbuton= driver.findElement(By.name("btnK"));
	  searchbuton.click();
	  Thread.sleep(1000);

	  driver.navigate().refresh();
	  Thread.sleep(1000);

	  driver.navigate().back();
	  Thread.sleep(1000);

	  driver.navigate().forward();
	  Thread.sleep(1000);

	  driver.navigate().to("https://www.Amazon.com");

	  Thread.sleep(1000);

	  driver.navigate().back();

  }

  @AfterMethod
  public void afterMethod() {
	  driver.quit();

  }

}




