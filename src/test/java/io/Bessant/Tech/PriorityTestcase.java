package io.Bessant.Tech;

import org.testng.annotations.Test;


import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;


@Listeners(listen.Listen2.class)			

public class PriorityTestcase {
	WebDriver driver;
	  @BeforeMethod
	  public void beforeMethod() {
		  System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));
			    driver = new ChromeDriver();
				driver.get("https://www.google.com");

		  System.err.println("System initiated the driver");
		  
	  }
	  @Test(priority=0)
	  public void launchGoogle() throws  AWTException, InterruptedException {
		  
		  String s1="Google";
			
			String s2=driver.getTitle();
			Assert.assertEquals(s1, s2);			
	    Thread.sleep(1000);
			

		
	  }
	  
	  @Test(priority=1)
	  public void Searchword() throws InterruptedException, AWTException {

			Thread.sleep(1000);
			driver.findElement(By.id("lst-ib")).sendKeys("amazon");
			Thread.sleep(1000);

			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ESCAPE);
			robot.keyRelease(KeyEvent.VK_ESCAPE);
			WebElement searchbuton= driver.findElement(By.name("btnK"));
			searchbuton.click();
			Thread.sleep(1000);
	  }

	  @AfterMethod
	  public void afterMethod() {
		  
			driver.quit();

	  }
}
