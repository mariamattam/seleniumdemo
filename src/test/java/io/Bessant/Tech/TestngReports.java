package io.Bessant.Tech;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

public class TestngReports {

	static ExtentReports extent=null;
	static ExtentTest logger=null;
	@BeforeClass
	public void beforeclass()
	{
		extent = new ExtentReports (System.getProperty("user.dir") +"/test-output/STMExtentReport.html", true);
		extent
                .addSystemInfo("Host Name", "Tournament OS")
                .addSystemInfo("Environment", "Automation Testing")
                .addSystemInfo("User Name", "Ganesh S");
                extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));

		
	}
	

	WebDriver driver;
  @BeforeMethod
  public void beforeMethod() {
	  
	  
	        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));
		    driver = new ChromeDriver();
			driver.get("https://www.google.com");
            System.err.println("System initiated the driver");
	  
  }
  @Test
  public void launchGoogle() throws InterruptedException, AWTException {
	  
		logger=extent.startTest("launchGoogle");
		
		logger.log(LogStatus.INFO, "Trying to execute the test case launchGoogle");

	  String s1="Google";
		
		String s2=driver.getTitle();
		Assert.assertEquals(s1, s2);			
    Thread.sleep(1000);
	logger.log(LogStatus.INFO, "Title of the screen is"+s2);

	logger.log(LogStatus.PASS, "this test method is passed");
	
  }
  
  @Test
  public void Searchword() throws InterruptedException, AWTException {

		logger=extent.startTest("Searchword");
		logger.log(LogStatus.INFO, "Trying to execute the test case searchword");

		Thread.sleep(1000);
		driver.findElement(By.id("lst-ib")).sendKeys("amazon");
		Thread.sleep(1000);

		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ESCAPE);
		robot.keyRelease(KeyEvent.VK_ESCAPE);
		WebElement searchbuton= driver.findElement(By.name("btnK"));
		searchbuton.click();
		Thread.sleep(1000);
		logger.log(LogStatus.PASS, "this test method is passed");


  }

  
  
  @Test
  public void Searchord() throws InterruptedException, AWTException {
	  logger=extent.startTest("Searchord");

	  logger.log(LogStatus.FAIL, "this test method is failed");
		logger.log(LogStatus.FAIL, logger.addScreenCapture("F:\\tournamentosautomation\\Tech\\SCREENSHOT\\Screen1.png"));
	  
		Thread.sleep(1000);
		// invalid find element id  (so test case will be failed)
		driver.findElement(By.id("lghfghfghfghfst-ib")).sendKeys("amazon");
		
		

		
		Thread.sleep(1000);

		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ESCAPE);
		robot.keyRelease(KeyEvent.VK_ESCAPE);
		WebElement searchbuton= driver.findElement(By.name("btnK"));
		searchbuton.click();
		Thread.sleep(1000);
		

  }
  
  

  @Test
  public void GmailLogin() throws InterruptedException, AWTException {
		
	  logger=extent.startTest("Searchord");

	  logger.log(LogStatus.SKIP, "this test method is skipped");

		throw new SkipException("skipped");
		
		
  }
  
  
  @AfterMethod
  public void afterMethod() {
	  
	  extent.endTest(logger);

		driver.quit();

  }
  @AfterClass
  public void afterclass()
  {   
	  
	  extent.flush();
		extent.close();

  }
}
