package io.Bessant.Tech;

import org.testng.annotations.Test;


import org.testng.annotations.BeforeMethod;
import org.testng.annotations.ExpectedExceptions;
import org.testng.annotations.Parameters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;

public class Test1 {
	
  @Parameters({"browser"})
  //enabled=false
  @Test()
  public void FirstTestcase(String browsername) {
	  
	  System.err.println("System is running first Test case"+browsername);
  }
  
  @Test()
  public void Temporary() {
	  System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));
	  WebDriver  driver = new ChromeDriver();
		driver.get("https://www.google.com");
	driver.getWindowHandle();

System.err.println("System initiated the driver");

	  System.err.println("system is running Temporary Test case");
  }
  @BeforeMethod
  public void beforeMethod() {
	  System.err.println("system is going to execute first test case");

  }

  @AfterMethod
  public void afterMethod() {
	  
	  System.err.println("system Executed first test case");

  }

}
