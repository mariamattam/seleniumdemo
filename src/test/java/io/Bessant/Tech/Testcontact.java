package io.Bessant.Tech;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.apache.poi.ss.usermodel.Cell;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;

public class Testcontact {
	 WebDriver driver;
  @BeforeMethod
  public void beforeMethod() {
	  System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));
       driver = new ChromeDriver();
  }
  
  @Test(dataProvider="contactDetails",dataProviderClass=io.Bessant.Tech.Dataprovider.class)
  public void contactdetailsReg(String name,String emailaddress,String subject,String message)
  
  {
	
      driver.get("http://demoqa.com/");
		driver.manage().window().maximize();

		driver.findElement(By.linkText("Contact")).click();
		driver.findElement(By.name("your-name")).sendKeys(name);
		driver.findElement(By.name("your-email")).sendKeys(emailaddress);
       driver.findElement(By.name("your-subject")).sendKeys(subject);
	   driver.findElement(By.name("your-message")).sendKeys(message);
		driver.findElement(By.xpath("//*[@id=\"wpcf7-f375-p28-o1\"]/form/p[5]/input")).click();

		
	  
  }
  

  @AfterMethod
  public void afterMethod() {
	  
	  driver.quit();
  }

}
