package ProjectPractice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DemocontactusPage {
  
	WebDriver driver;
	
	@FindBy(name="your-name")
	WebElement Name;
	
	@FindBy(name="your-email")
	WebElement Email;
	
	@FindBy(name="your-subject")
	WebElement Subject;
	
	@FindBy(name="your-message")
	WebElement Message;
	
	@FindBy(xpath="//*[@id=\\\"wpcf7-f375-p28-o1\\\"]/form/p[5]/input")
	WebElement clickable;
	

public DemocontactusPage(WebDriver driver) {
		
		this.driver=driver;
		PageFactory.initElements(driver, this);

	}
	
	
	public DemocontactusPage() {
}


	public void SubmitContact(String name,String emailaddress,String subject,String message) throws InterruptedException
	{
		
		Thread.sleep(500);
		Name.sendKeys(name);
		Thread.sleep(500);

		Email.sendKeys(emailaddress);
		Thread.sleep(500);

		Subject.sendKeys(subject);
		Thread.sleep(500);

		Message.sendKeys(message);
		Thread.sleep(500);

		clickable.click();

		
	}
	
	
			
	
	
}
