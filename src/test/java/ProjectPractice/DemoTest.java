package ProjectPractice;

import org.testng.annotations.Test;

import io.Bessant.Tech.Dataprovider;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;

public class DemoTest {
	WebDriver driver;
	DeomLandingPage dl;
	DemocontactusPage demo;
 @BeforeMethod
	  public void beforeMethod() 
 
 {
	 System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir").concat("\\Drivers\\chromedriver.exe"));
     driver = new ChromeDriver();
     driver.get("http://demoqa.com/");
		driver.manage().window().maximize();	  }
	
  @Test(dataProvider="positivedata")
  public void Checkcontactus_post(String name,String emailaddress,String subject,String message) throws InterruptedException {
	  
	//  dl=new DeomLandingPage(driver);
		dl=new DeomLandingPage(driver);  
		
	 demo=  dl.clicRegistration();
			  
	  demo.SubmitContact(name, emailaddress, subject, message);
  }

  @AfterMethod
  public void afterMethod() {
  }

  @DataProvider
	public static Object[][] positivedata() throws IOException
	
	{
	  
	String data[][]=new Dataprovider().getdataprovider(System.getProperty("user.dir").concat("\\EXCEL\\input.xlsx"), "contactus", 4);
		
	
	
	  return new Object[][] {
	      new Object[] { data[1][0], data[1][1],data[1][2],data[1][3] },
	    };		
	}
	
  
}
