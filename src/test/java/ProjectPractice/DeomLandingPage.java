package ProjectPractice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.ExpectedExceptions;

public class DeomLandingPage {
	
	WebDriver driver;
	WebDriverWait wait;
	
	@FindBy(linkText="Contact")
	WebElement contactus;
	
	
	public DeomLandingPage(WebDriver driver) {
	
	
		
		this.driver=driver;
		PageFactory.initElements(driver, this);
		
	}




	public String getTitle()
	{
		String title=driver.getTitle();
		return title;
	}
	
	public DemocontactusPage clicRegistration()
	{  
		contactus.click();	
		return new DemocontactusPage(driver);
		
	}

}
